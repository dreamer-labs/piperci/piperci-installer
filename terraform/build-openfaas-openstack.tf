# Installation
#   Run ./install-tf-ansible.sh -v 2.2.0

variable "tags" { default = "" }
variable "instance_namespace" { default = "openfaas" }
variable "run_provisioner" { default = true }
variable "key_pair" { default = "gitlab" }
variable "image_id" { default = "000cc5ab-f4e0-4ed3-a000-54148420eedb"}

# Configure the Openstack Provider
provider "openstack" {
}

resource "openstack_compute_instance_v2" "openfaas" {
  image_id  = "${var.image_id}"
  name   = "openfaas-1-${var.instance_namespace}"
  flavor_name = "s1-4"
  security_groups = ["default", "${openstack_compute_secgroup_v2.piperci_public.id}"]
  key_pair = "${var.key_pair}"

  block_device {
    uuid = "${var.image_id}"
    source_type = "image"
    destination_type = "local"
    boot_index = 0
    delete_on_termination = true
  }

  block_device {
    uuid = "${openstack_blockstorage_volume_v2.gman.id}"
    source_type = "volume"
    destination_type = "volume"
    boot_index = 1
  }

  block_device {
    uuid = "${openstack_blockstorage_volume_v2.minio.id}"
    source_type = "volume"
    destination_type = "volume"
    boot_index = 2
  }
  user_data = "${data.template_file.script.rendered}"

  network {
    name = "Ext-Net"
  }
}

data "template_file" "script" {
  template = "${file("user-data.sh.tpl")}"
  vars {
    minio_device_id = "${openstack_blockstorage_volume_v2.minio.id}"
    gman_device_id = "${openstack_blockstorage_volume_v2.gman.id}"
    mount_volumes = "${var.run_provisioner ? 0 : 1}"
  }
}

resource "openstack_blockstorage_volume_v2" "gman" {
  name = "gman"
  size = 30
}

resource "openstack_blockstorage_volume_v2" "minio" {
  name = "minio"
  size = 50
}

resource "openstack_compute_secgroup_v2" "piperci_public" {
  name        = "piperci_public"
  description = "Access to public PiperCI services"

  rule {
    from_port   = 8080
    to_port     = 8080
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }

  rule {
    from_port   = 8089
    to_port     = 8089
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
  rule {
    from_port   = 9000
    to_port     = 9000
    ip_protocol = "tcp"
    cidr        = "0.0.0.0/0"
  }
}

# Rerun provisioners on clean resources with null_resource
resource "null_resource" "ansible_config" {
  count = "${var.run_provisioner ? 1 : 0}"

  triggers {
    rerun = "${uuid()}"
  }

  # Bootstrap script can run on any instance of the openfaas droplet
  # So we just choose the first in this case
  connection {
    host = "${openstack_compute_instance_v2.openfaas.access_ip_v4}"
    user = "ubuntu"
  }

  provisioner "ansible" {
    plays {
      playbook = {
        file_path = "../piperci-stack.yml"
        roles_path = ["../roles"]
        # tags = [ "${var.tags}" ]
      }
      # shared attributes
      enabled = true
      become = true

      extra_vars = {
        ansible_python_interpreter = "python3"
        pi_install_swarm_ip = "${openstack_compute_instance_v2.openfaas.access_ip_v4}"
      }
    }
  }
}

output "IPAddress" {
  # Get the IPv4 addresses
  value = "${openstack_compute_instance_v2.openfaas.access_ip_v4}"
}

output "InstanceName" {
  # Get Instance Name
  value = "${openstack_compute_instance_v2.openfaas.name}"
}

output "OpenFaas_URI" {
  value = "http://${openstack_compute_instance_v2.openfaas.access_ip_v4}:8080/ui/"
  description = "OpenFaas Service URI"
  depends_on = [
    # Only output the openfaas info if the ansible configs ran successfully
    "null_resource.ansible_config",
  ]
}
