#!/bin/sh

if [ ${mount_volumes} -eq 1 ] ; then
  gman_device_id=${gman_device_id}
  gman_device=$(readlink -f $(ls -d -1 /dev/disk/by-id/* | grep "$${gman_device_id:0:5}"))
  minio_device_id=${minio_device_id}
  minio_device=$(readlink -f $(ls -d -1 /dev/disk/by-id/* | grep "$${minio_device_id:0:5}"))

  mkfs.ext4 -F -L GMAN $gman_device
  mkfs.ext4 -F -L MINIO $minio_device

  grep -qF 'LABEL=GMAN' /etc/fstab || echo "LABEL=GMAN  /data/  ext4  defaults  0 0" >> /etc/fstab
  grep -qF 'LABEL=MINIO' /etc/fstab || echo "LABEL=MINIO  /mnt/  ext4  defaults  0 0" >> /etc/fstab

  mkdir -p /data
  mkdir -p /mnt

  mount -a
fi