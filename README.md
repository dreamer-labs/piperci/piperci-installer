# PiperCI Installer

[![Build Status](https://gitlab.com/dreamer-labs/piperci/piperci-installer/badges/master/pipeline.svg)](https://gitlab.com/dreamer-labs/piperci/piperci-installer)

## Purpose

This repository contains the code required to setup various environments used to deploy Piper CI for production and CI usage.

## Install Targets

### Vagrant (virtualbox)

For a quick easy install a Vagrant file has been created in `dev/`. We've defaulted to using the virtualbox provider as it is the most common cross platform virtualization option for workstations. However it should work with only minor modifications on kvm or hyperv.

Requirements:

```
Vagrant >= 2.2.7
ansible >= 2.7
```

to use:

```bash
cd dev/
vagrant up --provider virtualbox
cat ../install-info.md
```

### OpenStack with Terraform

The suggested way to provision an environment is with Terraform, however you can also deploy a PiperCI installation using only the Ansible role

#### Configure your environment for Terraform deployments

1. Install Ansible

  `pip3 install ansible==2.7`

1. Install Terraform

1. Install the tf ansible plugin

  `bash install-tf-ansible.sh -v 2.3.3`

1. Initialized Terraform

  `terraform init`

1. Configure Terraform tfvars

  Configure your terraform.tfvars file with your key hash. Also ensure your openstackrc file has been sourced. See terraform/terraform.tfvars.example

1. (Optional) Define a non-default PiperCI Release

  The PiperCI Releases repository contains configuration for your installation. This includes things like package versions,
  repository versions, and FaaS Functions.

  The piperci-releases repository is defined by the `read_releases_url` and `read_releases_version` environment variables. If these
  variables are not set then the installer role will use the following as defaults:

  ```
  read_releases_url: https://gitlab.com/dreamer-labs/piperci/piperci-releases.git
  read_releases_version: master
  ```

#### Run Terraform

```bash
cd terraform/
terraform apply
```

1. `cat ../install-info.md`

### Accessing your deployed environment

The environment info will be dumped to the CWD of the ansible process(usually repo root). It will contain info about installed versions, port mappings and available services.

## Deploying your OpenFaaS Functions

The `piperci-stack.yml` playbook will pull FaaS definitions from the PiperCI-Releases repository defined in the `read_releases_*` environment
variables. It utilizes the `read_releases` role to parse the installer release file and transform these YAML definitions into Ansible variables.

### Define inside PiperCI Releases

A production installation of PiperCI will have all of its release information defined in an external PiperCI Releases repository.

```
installer:
  faas:
    - name: noop-function
      functions:
        - name: "piperci-noop"
          image: "registry.gitlab.com/dreamer-labs/piperci/piperci-noop-faas"
```

To define a new FaaS function deploy inside of your PiperCI environment you would just add an additional list item. For example, if you wanted to define a "piperci-flake8" function here you would add the list item to the `faas` dict.

```
installer:
  faas:
    - name: flake8-function
      functions:
        - name: "piperci-flake8"
          image: "registry.gitlab.com/dreamer-labs/piperci/piperci-flake8-faas"
```

Then, you would rerun the `configure.yml` playbook.

### Or modify the playbook

If you are not using PiperCI Releases to control your versioning information you can also modify the `piperci-stack.yml` playbook directly.
Usually you would do this when you are not using Terraform to deploy your infrastructure.

```
---
- hosts: all
  gather_facts: True
  roles:
    - role: ansible-role-pi_install
      ...
      pi_install_piperci_faas:
        - name: noop_faas
          functions:
            - name: piperci-noop-gateway
              image: "registry.gitlab.com/dreamer-labs/piperci/piperci-noop-faas/gateway:latest"
```

### Docker (Docker in Docker)

This Docker installation method requires Docker version 19.03 or greater.
This will create a Docker Stack on your machine with all the required components utilizing Ansible.

#### Requirements

```
  * Software:
    `Ansible >= 2.8`
    `Docker >= 19.03`
    `Docker (python) >=3.7.3`
  * `Disk: 25GB`
  * `CPU: 2vCPU`
  * `RAM: 2GB`
  * Also requires the ability to create privileged docker containers.
```

#### Installation

```
apt-get install python3-virtualenv
virtualenv -p python3 docker
source docker/bin/activate
pip install docker ansible
ansible-playbook ansible-dind.yml
```

## License

[LICENSE](https://gitlab.com/dreamer-labs/piperci/piperci-picli/blob/master/LICENSE)

## Authors

See the list of [contributors](https://gitlab.com/dreamer-labs/piperci/piperci-installer/-/graphs/master) who participated in this project.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags](https://gitlab.com/dreamer-labs/piperci/piperci-picli/-/tags) on this repository.

## Contributing

Please read [Contributing Guide](https://piperci.dreamer-labs.net/project-info/contributing) for details on our code of conduct, and the process for submitting pull requests.
