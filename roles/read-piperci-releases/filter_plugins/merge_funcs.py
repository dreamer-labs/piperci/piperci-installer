from ansible.errors import AnsibleFilterError

def merge_dicts(a, b, key=None):
    """
    Merges b into a
    :param a: A list of dictionaries
    :param b: A list of dictionaries
    :param key: The key to base the merge on
    :return:
    """
    if not key:
         raise AnsibleFilterError("merge_dicts filter must be passed a key")

    return {x[key]:x for x in a + b}

class FilterModule():

    def filters(self):
        filters = {
            'merge_dicts': merge_dicts
        }

        return filters