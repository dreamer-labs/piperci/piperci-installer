---
- name: this block contains tasks related to openfaas
  block:

    - name: ensure latest versions of openfaas/faas repo has been git cloned
      git:
        repo: "{{ pi_install_openfaas.repo }}"
        version: "{{ pi_install_openfaas.version }}"
        dest: "{{ pi_install_user.homedir }}/openfaas/faas/"
        force: "yes"
      register: clone_openfaas_faas

    - name: ensure docker-compose.yml is in user home
      copy:
        src: "{{ pi_install_user.homedir}}/openfaas/faas/docker-compose.yml"
        dest: "{{ pi_install_user.homedir }}/docker-compose.yml"
        owner: "{{ pi_install_user.name }}"
        group: "{{ pi_install_user.name }}"
        mode: "0644"
        remote_src: True
      when: clone_openfaas_faas is changed
      notify:
        - deploy stack

    - name: ensure manage_docker_secrets script is in path
      copy:
        src: "files/manage_docker_secrets.sh"
        dest: "/usr/local/bin/manage_docker_secrets"
        owner: "root"
        group: "root"
        mode: "0755"

    - name: ensure minio docker secrets are generated and stored
      shell: "manage_docker_secrets add {{ item }}"
      args:
        chdir: "{{ pi_install_user.homedir }}"
        executable: "/bin/bash"
        creates: "{{ pi_install_user.homedir }}/{{ item }}.docker_secret.txt"
      when: pi_install_secrets is not defined
      loop:
        - access-key
        - secret-key
      notify:
        - deploy stack

    - name: Check if docker secrets already exist
      shell: |
        "docker secret inspect {{ item.key }}"
      when: pi_install_secrets is defined
      loop: "{{ (pi_install_secrets | default({})) | dict2items }}"
      failed_when: False
      changed_when: False
      register: docker_secrets_created

    - name: create docker secrets
      shell: "printf {{ item.value }} | docker secret create {{ item.key }} -"
      when:
        - pi_install_secrets is defined
      loop: "{{ (pi_install_secrets | default ({}))| dict2items }}"
      register: test
      changed_when: "'AlreadyExists' not in test.stderr"
      failed_when:
        - test.rc != 0
        - (test.rc != 1 and 'AlreadyExists' not in test.stderr)

    - name: ensure minio settings are referenced in openfaas/faas docker-compose.yml file
      yedit:
        src: "{{ pi_install_user.homedir }}/openfaas/faas/docker-compose.yml"
        key: "{{ item.key }}"
        value: "{{ item.value }}"
      loop:
        - { key: secrets.access-key.external, value: "true" }
        - { key: secrets.secret-key.external, value: "true" }
        - { key: services.minio.deploy.restart_policy.delay, value: "10s" }
        - { key: services.minio.deploy.restart_policy.max_attempts, value: "10" }
        - { key: services.minio.deploy.restart_policy.window, value: "60s" }
        - { key: services.minio.hostname, value: "minio" }
        - { key: services.minio.image, value:  "{{ pi_install_minio['image'] }}"}
        - { key: services.minio.networks, value: ["functions"] }
        - { key: services.minio.ports, value: ["9000:9000"] }
        - { key: services.minio.command, value: "server /export" }
        - { key: services.minio.secrets, value: ["secret-key", "access-key"] }
        - { key: services.minio.volumes, value: ["minio_data:/export"] }
        - { key: services.minio.environment, value: ["MINIO_ACCESS_KEY_FILE=access-key", "MINIO_SECRET_KEY_FILE=secret-key"] }
        - { key: volumes.minio_data.driver, value: "local" }
        - { key: services.gman.deploy.restart_policy.delay, value: "10s" }
        - { key: services.gman.deploy.restart_policy.max_attempts, value: "10" }
        - { key: services.gman.deploy.restart_policy.window, value: "60s" }
        - { key: services.gman.hostname, value: "gman" }
        - { key: services.gman.image, value: "{{ pi_install_piperci_gman['image'] }}" }
        - { key: services.gman.networks, value: ["functions"] }
        - { key: services.gman.ports, value: ["8089:8080"] }
      vars:
        ansible_python_interpreter: python
      notify:
        - deploy stack

    - name: ensure docker swarm stack is deployed/updated (as required)
      meta: flush_handlers

    - pause:
        seconds: 30

  tags:
    - openfaas
...
